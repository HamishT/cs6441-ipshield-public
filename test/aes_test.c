#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

// Test macros are enabled in CMake configuration.
#include "logger.h"
#include "aes_lookup.h"
#include "crypto.h"

static unsigned char htob(char ch, char cl);
static void write_hex(char *dst, unsigned char *src, int len);

/*
 * Arg 0 is the input key in hex. If less that 32 chars, the rest is padded.
 * Arg 1 is the input string. It can be any length.
 */
int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: aes_test <key_hex> <input_hex>\n");
        return 1;
    }

    char *skey = argv[1];
    char *sin = argv[2];

    int skeylen = strlen(skey);
    int keylen = skeylen/2;
    int sinlen = strlen(sin);
    int inlen = sinlen/2;

    unsigned char *kbuf = calloc(keylen, 1);
    unsigned char *ibuf = malloc(inlen);
    if (kbuf == NULL || ibuf == NULL) {
        log_error("input buffer allocation failed");
        return 1;
    }


    for (int i = 0; i < 16 && i < inlen; i++) {
        kbuf[i] = htob(skey[2*i], skey[2*i+1]);
    }
    for (int i = 0; i < inlen; i++) {
        ibuf[i] = htob(sin[2*i], sin[2*i+1]);
    }

    aes_lut_initialise();

    unsigned char *encrypted = malloc(inlen+16);
    unsigned char *decrypted = malloc(inlen+16);
    if (encrypted == NULL || decrypted == NULL) {
        log_error("result buffer allocation failed");
        return 1;
    }

    crypto_state_t cs;
    memcpy(cs.key, kbuf, 16);
    log_info("Test main: initialising crypto state...");
    crypto_initialise(&cs);

    log_info("Running encrypt test...");
    int elen = crypto_encrypt(&cs, ibuf, inlen, encrypted);
    log_info("Encrypt returned %d bytes", elen);

    log_info("Running decrypt test...");
    int dlen = crypto_decrypt(&cs, encrypted, elen, decrypted);
    log_info("Decrypt returned %d bytes", dlen);

    for (int i = 0; i < 16; i++) {
        if (decrypted[i] != ibuf[i]) {
            log_error("Decrypted data did not match input. Exiting...");
            return 1;
        }
    }
    log_info("Decrypted data matched input data! Success!");

    char *plainbuf = malloc(dlen*2+1);
    char *cipherbuf = malloc(elen*2+1);
    if (plainbuf == NULL || cipherbuf == NULL) {
        log_error("output buffer allocation null");
        return 1;
    }
    write_hex(cipherbuf, encrypted, elen);
    write_hex(plainbuf, decrypted, dlen);
    log_info("Ciphertext is: %s", cipherbuf);
    log_info("Plaintext is: %s", plainbuf);


    free(kbuf);
    free(ibuf);
    free(cipherbuf);
    free(plainbuf);
    free(encrypted);
    free(decrypted);

}

static unsigned char htob(char ch, char cl) {
    ch = toupper(ch);
    cl = toupper(cl);

    ch = (ch > '9') ? ch - 'A' + 10 : ch - '0';
    cl = (cl > '9') ? cl - 'A' + 10 : cl - '0';

    return (ch << 4) | cl;
}

static void write_hex(char *dst, unsigned char *src, int len){
    for (int i = 0; i < len; i++) {
        unsigned char hi = src[i] >> 4;
        unsigned char lo = src[i] & (unsigned char) 0x0f;
        dst[2*i] = (hi > 9) ? hi + 'A' - 10 : hi + '0';
        dst[2*i+1] = (lo > 9) ? lo + 'A' - 10 : lo + '0';
    }
    dst[len*2] = '\0';
}