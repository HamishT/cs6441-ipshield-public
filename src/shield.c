/*
 * Main filtering and encryption program
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <ctype.h>
#include <netinet/in.h>
#include <linux/netfilter.h>
// The pktbuff authors used stdbool but didn't include it!
#include <stdbool.h>
#include <libnetfilter_queue/pktbuff.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libnetfilter_queue/libnetfilter_queue_ipv4.h>
#include <libnetfilter_queue/libnetfilter_queue_tcp.h>
#include <crypto.h>

#include "logger.h"
#include "crypto.h"
#include "queue_manager.h"
#include "aes_lookup.h"

static char cbuffer[0xffff];
static crypto_state_t cstate;
static volatile char should_exit = 0;

static void handle_interrupt(int signum);
static int packet_handle_common(int encryptNotDecrypt,
                                struct nfq_q_handle *qh,
                                struct nfgenmsg *nfmsg,
                                struct nfq_data *nfd,
                                void *data);
static int packet_handle_in(struct nfq_q_handle *qh,
                            struct nfgenmsg *nfmsg,
                            struct nfq_data *nfd,
                            void *data);
static int packet_handle_out(struct nfq_q_handle *qh,
                             struct nfgenmsg *nfmsg,
                             struct nfq_data *nfd,
                             void *data);

static void handle_interrupt(int signum) {
    should_exit = 1;
}

static unsigned char htob(char ch, char cl);

int main (int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: ipshield <key_hex>\n");
        return 1;
    }

    log_info("Initalising math lookup tables...");
    aes_lut_initialise();
    unsigned char *kbuf = calloc(16, 1);
    if (kbuf == NULL) {
        log_error("Could not allocate key buffer");
        return 1;
    }
    char *skey = argv[1];
    int skeylen = strlen(skey);
    int keylen = skeylen/2;
    for (int i = 0; i < 16 && i < keylen; i++) {
        kbuf[i] = htob(skey[2*i+0], skey[2*i+1]);
    }
    memcpy(cstate.key, kbuf, 16);
    log_info("Initialising crypto state...");
    crypto_initialise(&cstate);

    struct nfq_handle *h;
    struct nfq_q_handle *qhi, *qho;
    int socket;

    log_info("Attaching to filter queues...");
    if (queue_attach(&h, &packet_handle_in, &packet_handle_out, &qhi, &qho, &socket)) {
        log_error("Could not attach to queues, aborting...");
        exit(1);
    }

    // set up interrupt handler so we can exit at some point
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = &handle_interrupt;
    sigaction(SIGINT, &sa, NULL);

    ssize_t rv;
    int retval = 0;
    while (1) {
        // The flag gets checked twice because signals are untrustworthy and treacherous.
        if (should_exit) {
            log_info("Shield got standard exit flag. Exiting...");
            break;
        }
        rv = recv(socket, cbuffer, 0xffff, 0);
        if(should_exit) {
            log_info("Shield got standard exit flag. Exiting...");
            break;
        }
        if (rv == 0) {
            log_warn("Shield socket closed unexpectedly. Exiting...");
            break;
        } else if (rv == -1) {
            int err = errno;
            if (err == EINTR) {
                log_info("Shield receive interrupted. Exiting...");
            } else {
                log_error("Shield packet receive error: %s. Exiting...", strerror(err));
                retval = 1;
            }
            break;
        }
        nfq_handle_packet(h, cbuffer, rv);
    }

    log_info("Detaching from queues...");
    queue_detach(h, qhi, qho);
    return retval;
}

static unsigned char htob(char ch, char cl) {
    ch = toupper(ch);
    cl = toupper(cl);

    ch = (ch > '9') ? ch - 'A' + 10 : ch - '0';
    cl = (cl > '9') ? cl - 'A' + 10 : cl - '0';

    return (ch << 4) | cl;
}

static int packet_handle_common(int encryptNotDecrypt,
                                struct nfq_q_handle *qh,
                                struct nfgenmsg *nfmsg,
                                struct nfq_data *nfd,
                                void *data) {

    /*
     * Note relevant NFQ functions
     * Return statuses are annoyingly mixed:
     * N: NULL on fail, -1: -1 on fail, 0: always succeeds, 1: 0 on fail
     *
     *  N nfq_ip_get_hdr(pb)
     * -1 nfq_ip_set_transport_header(iph)
     *  N nfq_tcp_get_hdr(pb)
     *  N nfq_tcp_get_payload(tcph, pb)
     * -1 nfq_tcp_get_payload_len(tcph, pb)
     *  0 nfq_tcp_compute_checksum_ipv4(tcph, iph)
     *  1 nfq_tcp_mangle_ipv4(pb, offs, len0, data, len1)
     */

    uint32_t id;
    int rawlen, tcplen, newlen;
    unsigned char *rawbuf, *tcpbuf;
    struct nfqnl_msg_packet_hdr *hdr;
    struct pkt_buff *pbuf;
    struct iphdr *iph;
    struct tcphdr *tcph;

    hdr = nfq_get_msg_packet_hdr(nfd);
    if (hdr == NULL) {
        log_error("packet handler got a null message header");
        return -1;
    }

    id = ntohl(hdr->packet_id);

    rawlen = nfq_get_payload(nfd, &rawbuf);
    if (rawlen < 0) {
        log_error("packet handler did not get any raw data");
        return -1;
    }

    // 128 extra bytes in case of padding
    pbuf = pktb_alloc(AF_INET, rawbuf, rawlen, 128);
    if (pbuf == NULL) {
        log_error("packet handler could not allocate packet buffer");
        return -1;
    }

    iph = nfq_ip_get_hdr(pbuf);
    if (iph == NULL) {
        log_error("packet handler got a null ip header");
        return -1;
    }

    if (nfq_ip_set_transport_header(pbuf, iph) < 0) {
        log_error("packet handler could not set transport header");
        return -1;
    }

    tcph = nfq_tcp_get_hdr(pbuf);
    if (tcph == NULL) {
        log_error("packet handler could not get transport header");
        return -1;
    }

    tcpbuf = nfq_tcp_get_payload(tcph, pbuf);
    if (tcpbuf == NULL) {
        log_error("packet handler could not get TCP payload");
        return -1;
    }

    tcplen = nfq_tcp_get_payload_len(tcph, pbuf);
    if (tcplen < 0) {
        log_error("packet handler did not get any TCP data");
        return -1;
    }

    // don't mangle empty packets (it adds padding)
    if (tcplen > 0) {
        int (*func)(crypto_state_t*, void*, int, void*) = (encryptNotDecrypt) ? &crypto_encrypt : &crypto_decrypt;
        newlen = func(&cstate, tcpbuf, tcplen, cbuffer);

        // note this function returns positive on success and 0 on fail
        if (!nfq_tcp_mangle_ipv4(pbuf, 0, tcplen, cbuffer, newlen)) {
            log_error("packet handler TCP data mangle failed");
            return -1;
        }
    }

    if (nfq_set_verdict(qh, id, NF_ACCEPT, pktb_len(pbuf), pktb_data(pbuf)) < 0) {
        log_error("packet handler verdict set failed");
        return -1;
    }

    pktb_free(pbuf);
    return 0;
}

static int packet_handle_in(struct nfq_q_handle *qh,
                            struct nfgenmsg *nfmsg,
                            struct nfq_data *nfd,
                            void *data) {
    return packet_handle_common(0, qh, nfmsg, nfd, data);
}

static int packet_handle_out(struct nfq_q_handle *qh,
                             struct nfgenmsg *nfmsg,
                             struct nfq_data *nfd,
                             void *data) {
    return packet_handle_common(1, qh, nfmsg, nfd, data);
}