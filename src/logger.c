//
// Created by hamish on 2/05/17.
//

#include <stdio.h>
#include "logger.h"

static void log_common(FILE *f, const char *tag, const char *msg, va_list args){
    char s[1024];
    vsnprintf(s, 1024, msg, args);
    fprintf(f, "%s%s\n", tag, s);
}

void log_info(const char *msg, ...){
    va_list args;
    va_start(args, msg);
    log_common(stdout, " INFO: ", msg, args);
    va_end(args);
}

void log_debug(const char *msg, ...) {
    va_list args;
    va_start(args, msg);
    log_common(stdout, "DEBUG: ", msg, args);
    va_end(args);
}

void log_warn(const char *msg, ...){
    va_list args;
    va_start(args, msg);
    log_common(stderr, " WARN: ", msg, args);
    va_end(args);
}

void log_error(const char *msg, ...){
    va_list args;
    va_start(args, msg);
    log_common(stderr, "ERROR: ", msg, args);
    va_end(args);
}