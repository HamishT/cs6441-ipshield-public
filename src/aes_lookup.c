#include "aes_lookup.h"

// memory allocations: see header file for usage details
unsigned char aes_rcon[11];
unsigned char aes_sbox[256];
unsigned char aes_sbox_inv[256];
unsigned char aes_log[256];
unsigned char aes_alog[256];
unsigned char aes_mul2[256];
unsigned char aes_mul3[256];
unsigned char aes_mul9[256];
unsigned char aes_mulb[256];
unsigned char aes_muld[256];
unsigned char aes_mule[256];
unsigned char aes_mul_inv[256];

unsigned char aes_rcon[11] = {
        0x8d, 0x01, 0x02, 0x04, 0x08,
        0x10, 0x20, 0x40, 0x80, 0x1b,
        0x36,
};

void aes_lut_initialise(void) {
    // generate logarithms using 3 as a generator
    // this forms the basis for all other calculations
    unsigned char p = 1;
    unsigned char h;
    for (int i = 0; i < 256; i++) {
        // multiply (in galois field) p by 3
        aes_alog[i] = p;
        h = p & 0x80;
        p <<= 1;
        if (h)
            p ^= 0x1b;
        p ^= aes_alog[i];
        aes_log[aes_alog[i]] = i;
    }
    aes_alog[255] = aes_alog[0];
    aes_log[0] = 0;

    // initialise common tables
    for (int i = 0; i < 256; i++) {
        aes_mul2[i] = aes_multiply(2, i);
        aes_mul3[i] = aes_multiply(3, i);
        aes_mul9[i] = aes_multiply(9, i);
        aes_mulb[i] = aes_multiply(11, i);
        aes_muld[i] = aes_multiply(13, i);
        aes_mule[i] = aes_multiply(14, i);
        aes_mul_inv[i] = aes_divide(1, i);
    }

    // initialise s-box and its inverse
    // this is an alternate way to perform the affine transform operation
    for (int i = 0; i < 256; i++) {
        unsigned char s = aes_mul_inv[i];
        unsigned char result = s;
        for (int j = 0; j < 4; j++) {
            s = (s << 1) | (s >> 7); // rotate left 1 bit
            result ^= s;
        }
        result ^= 0x63;
        aes_sbox[i] = result;
        aes_sbox_inv[result] = i;
    }
}

/*
 * How this works:
 * The log table, for entry x, represents log(g,x) for a generator number (for us, 3).
 * The alog table, for entry x, represents g^x.
 * So e.g. if log(a) = 7 and log(b) = 2, then log(a*b) = 7+2 = 9 (from logarithm laws).
 * And the 9th entry would correctly give us 3^9, i.e. 3^7 * 3^2.
 */
unsigned char aes_multiply(unsigned char a, unsigned char b) {
    if (a == 0 || b == 0)
        return 0;
    // type forces the index to overflow as an 8-bit value
    unsigned logapbu = aes_log[a] + aes_log[b];
    unsigned char logapbc = (unsigned char) (logapbu % 255);
    return aes_alog[logapbc];
}

/*
 *
 */
unsigned char aes_divide(unsigned char a, unsigned char b) {
    // divide by zero is defined as zero apparently
    if (a == 0 || b == 0)
        return 0;
    unsigned char loga = (a == 1) ? 255 : aes_log[a];
    unsigned char logamb = loga - aes_log[b];
    return aes_alog[logamb];
}
