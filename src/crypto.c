/*
 * AES algorithm implementation
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "crypto.h"
#include "logger.h"
#include "aes_lookup.h"

static void aes_encrypt(crypto_state_t *cs);
static void aes_decrypt(crypto_state_t *cs);

static void expand_key(crypto_state_t *cs);

static void add_round_key(crypto_state_t *cs, unsigned char *subkey);

static void substitute_bytes(void *dst, size_t len);
static void shift_rows(crypto_state_t *cs);
static void mix_columns(crypto_state_t *cs);

static void substitute_bytes_inverse(void *dst, size_t len);
static void shift_rows_inverse(crypto_state_t *cs);
static void mix_columns_inverse(crypto_state_t *cs);

static inline uint32_t join_bytes(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3);
static inline uint8_t *as8(void *array);
static inline uint32_t *as32(void *array);
static inline uint64_t *as64(void *array);

static void store_step(unsigned char *matrix, const char *where, int sub_num, int sub_total);
static void check_step(unsigned char *matrix, const char *where);
static void print_matrix(unsigned char *matrix);

/*
 * Debugging data.
 * Encryption stages are recorded for every encrypted block,
 * then played back and checked against decryption stages.
 * Store/check macros and log storage will only be enabled if the test macro is set.
 */

// If test macro set, enable store/check and allocate blocks for logging.
#ifdef IPSHIELD_CRYPTO_SELF_TEST
#define CRYPTO_STORE(m,w,p,q) store_step(m,w,p,q)
#define CRYPTO_CHECK(m,w) check_step(m,w)
#define __BLOCKS_MAX 32
#else
#define CRYPTO_STORE(m,w,p,q)
#define CRYPTO_CHECK(m,w)
#define __BLOCKS_MAX 0
#endif

#define __STEPS_MAX 64
#define __STEP_NAME_MAX 128
typedef struct {
    char step_name[__STEP_NAME_MAX];
    unsigned char step_matrix[16];
    int step_sub_num;
    int step_sub_total;
} step_t;
typedef struct {
    int block_num_steps;
    step_t block_steps[__STEPS_MAX];
} block_t;
static int block_count_enc = 0;
static int block_count_dec = 0;
static block_t blocks[__BLOCKS_MAX];
static block_t *curr_block = NULL;

/*
 * Encrypt with ECB chaining (basically no chaining).
 * Padding is PKCS5.
 */
int crypto_encrypt(crypto_state_t *cs, void *src, int len, void *dst) {
#ifdef IPSHIELD_CRYPTO_SELF_TEST
    block_count_enc = 0;
#endif
    unsigned char *din = (unsigned char *) src;
    unsigned char *dout = (unsigned char *) dst;
    int remaining = len;
    while (remaining >= 0) {
#ifdef IPSHIELD_CRYPTO_SELF_TEST
        if (block_count_enc == __BLOCKS_MAX) {
            log_error("Self test block limit reached. Exiting...");
            exit(1);
        }
        log_debug("Setting test log for encryption block %d", block_count_enc);
        curr_block = &(blocks[block_count_enc++]);
        curr_block->block_num_steps = 0;
#endif
        if (remaining < 16) {
            memcpy(cs->state, din, remaining);
            int pad = 16 - remaining;
            memset(cs->state + remaining, pad, pad);
        } else {
            memcpy(cs->state, din, 16);
        }
        aes_encrypt(cs);
        memcpy(dout, cs->state, 16);
        // advance block
        remaining -= 16;
        din += 16;
        dout += 16;
    }
    // ADDS however many bytes were used for padding (remaining is negative)
    return len - remaining;
}

/*
 * Decrypt with ECB (basically no chaining).
 * Padding is PKCS5.
 */
int crypto_decrypt(crypto_state_t *cs, void *src, int len, void *dst) {
#ifdef IPSHIELD_CRYPTO_SELF_TEST
    block_count_dec = 0;
#endif
    if (len < 16) {
        log_error("Decrypt must receive at least 16 bytes to decrypt");
        return -1;
    }
    unsigned char *din = (unsigned char *) src;
    unsigned char *dout = (unsigned char *) dst;
    int remaining = len;
    while (1) {
        if (remaining < 16) {
            log_error("decrypt padding violation (%d bytes remaining, expected 16)", remaining);
            return -1;
        }
#ifdef IPSHIELD_CRYPTO_SELF_TEST
        if (block_count_dec == block_count_enc) {
            log_error("More decrypt than encrypt blocks found during self test. Exiting...");
            exit(1);
        }
        log_debug("Getting test log for decryption block %d", block_count_dec);
        curr_block = &(blocks[block_count_dec++]);
        if (curr_block->block_num_steps == 0) {
            log_error("Test log block %d has no recorded data. Exiting...");
            exit(1);
        }
#endif
        memcpy(cs->state, din, 16);
        aes_decrypt(cs);
        memcpy(dout, cs->state, 16);
        remaining -= 16;
        din += 16;
        dout += 16;
        if (remaining == 0) {
            // At least one terminating byte must be padding.
            // Check there are N terminating bytes with value N.
            unsigned char pad = cs->state[15];
            for (int i = 1; i < pad; i++) {
                if (cs->state[15-i] != pad) {
                    log_error("decrypt padding violation (final state does not have valid padding with value %u", pad);
                    return -1;
                }
            }
#ifdef IPSHIELD_CRYPTO_SELF_TEST
            if (block_count_dec != block_count_enc) {
                log_error("Block count mismatched. %d blocks encrypted but only %d decrypted. Exiting...",
                          block_count_enc, block_count_dec);
                exit(1);
            }
#endif
            return len - pad;
        }
    }
}

void crypto_initialise(crypto_state_t *cs) {
    expand_key(cs);
}

static void expand_key(crypto_state_t *cs) {
    uint32_t temp;
    uint32_t *key_words = as32(cs->roundKeys);

    // round 0 is just the base key
    memcpy(cs->roundKeys[0], cs->key, 16);

    // 11 round keys * 16 bytes = 176 bytes = 44 words
    for (int i = 4; i < 44; i++) {
        temp = key_words[i-1];
        // on word 0 of each key...
        // rotate, substitute, add power to byte 0
        if (i % 4 == 0) {
            temp = (temp >> 8) | (temp << 24);
            uint8_t *bytes = as8(&temp);
            for (int b = 0; b < 4; b++) {
                bytes[b] = aes_sbox[bytes[b]];
            }
            bytes[0] ^= aes_rcon[i/4];
        }
        key_words[i] = key_words[i-4] ^ temp;
    }
}

static void aes_encrypt(crypto_state_t *cs) {
    CRYPTO_STORE(cs->state, "initial state", 1, 1);
    add_round_key(cs, cs->roundKeys[0]);
    for (int r = 1; r <= 10; r++) {
        CRYPTO_STORE(cs->state, "add round key", r, 10);
        substitute_bytes(cs->state, 16);
        CRYPTO_STORE(cs->state, "byte substitution", r, 10);
        shift_rows(cs);
        CRYPTO_STORE(cs->state, "row shift", r, 10);
        if (r != 10) {
            mix_columns(cs);
            CRYPTO_STORE(cs->state, "column mix", r, 10);
        }
        add_round_key(cs, cs->roundKeys[r]);
    }
    CRYPTO_STORE(cs->state, "add final round key", 1, 1);
}

static void aes_decrypt(crypto_state_t *cs) {
    CRYPTO_CHECK(cs->state, "check final");
    for (int r = 10; r > 0; r--) {
        add_round_key(cs, cs->roundKeys[r]);
        CRYPTO_CHECK(cs->state, "after reverse round key");
        if (r != 10) {
            mix_columns_inverse(cs);
            CRYPTO_CHECK(cs->state, "after inverse column mix");
        }
        shift_rows_inverse(cs);
        CRYPTO_CHECK(cs->state, "after shift rows inverse");
        substitute_bytes_inverse(cs->state, 16);
        CRYPTO_CHECK(cs->state, "after inverse byte substitution");
    }
    add_round_key(cs, cs->roundKeys[0]);
    CRYPTO_CHECK(cs->state, "final decrypted state");
}

static void add_round_key(crypto_state_t *cs, unsigned char *subkey) {
    uint64_t *state = as64(cs->state);
    uint64_t *key = as64(subkey);
    state[0] ^= key[0];
    state[1] ^= key[1];
}

static void substitute_bytes(void *dst, size_t len) {
    uint8_t *bytes = as8(dst);
    for (int i = 0; i < len; i++) {
        bytes[i] = aes_sbox[bytes[i]];
    }
}

static void substitute_bytes_inverse(void *dst, size_t len) {
    uint8_t *bytes = as8(dst);
    for (int i = 0; i < len; i++) {
        bytes[i] = aes_sbox_inv[bytes[i]];
    }
}

static void shift_rows(crypto_state_t *cs) {
    /* State matrix is column-major so:
     *
     * 0 4 8 C
     * 1 5 9 D
     * 2 6 A E
     * 3 7 B F
     *
     * becomes...
     *
     * 0 4 8 C
     * 5 9 D 1
     * A E 2 6
     * F 3 7 B
     */

    uint8_t t[16];
    memcpy(t, cs->state, 16);
    uint32_t *s = as32(cs->state);

    s[0] = join_bytes(t[0x0], t[0x5], t[0xa], t[0xf]);
    s[1] = join_bytes(t[0x4], t[0x9], t[0xe], t[0x3]);
    s[2] = join_bytes(t[0x8], t[0xd], t[0x2], t[0x7]);
    s[3] = join_bytes(t[0xc], t[0x1], t[0x6], t[0xb]);
}

static void shift_rows_inverse(crypto_state_t *cs) {
    /*
     * State matrix is column-major so...
     *
     * 0 4 8 C
     * 1 5 9 D
     * 2 6 A E
     * 3 7 B F
     *
     * becomes...
     *
     * 0 4 8 C
     * D 1 5 9
     * A E 2 6
     * 7 B F 3
     */

    uint8_t t[16];
    memcpy(t, cs->state, 16);
    uint32_t *s = as32(cs->state);

    s[0] = join_bytes(t[0x0], t[0xd], t[0xa], t[0x7]);
    s[1] = join_bytes(t[0x4], t[0x1], t[0xe], t[0xb]);
    s[2] = join_bytes(t[0x8], t[0x5], t[0x2], t[0xf]);
    s[3] = join_bytes(t[0xc], t[0x9], t[0x6], t[0x3]);
}

static uint32_t join_bytes(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3) {
    return b0 | b1<<8 | b2<<16 | b3<<24;
}

static void mix_columns(crypto_state_t *cs) {
    uint8_t *col;
    unsigned char t[4];
    unsigned char *m2 = aes_mul2;
    unsigned char *m3 = aes_mul3;
    for (int i = 0; i < 4; i++) {
        // copy column entries to t using fancy word pointer tricks
        col = &(cs->state[i*4]);
        *as32(t) = *as32(col);
        /*
         * The mix matrix is:
         * |2 3 1 1|
         * |1 2 3 1|
         * |1 1 2 3|
         * |3 1 1 2|
         */
        col[0] = m2[t[0]] ^ m3[t[1]] ^    t[2]  ^    t[3];
        col[1] =    t[0]  ^ m2[t[1]] ^ m3[t[2]] ^    t[3];
        col[2] =    t[0]  ^    t[1]  ^ m2[t[2]] ^ m3[t[3]];
        col[3] = m3[t[0]] ^    t[1]  ^    t[2]  ^ m2[t[3]];
    }
}

static void mix_columns_inverse(crypto_state_t *cs) {
    // see mix_columns(...) comments: basically the same
    uint8_t *col;
    unsigned char t[4];
    unsigned char *m9 = aes_mul9;
    unsigned char *mb = aes_mulb;
    unsigned char *md = aes_muld;
    unsigned char *me = aes_mule;
    for (int i = 0; i < 4; i++) {
        col = &(cs->state[i*4]);
        *as32(t) = *as32(col);
        /*
         * The inverse mix matrix is:
         * |E B D 9|
         * |9 E B D|
         * |D 9 E B|
         * |B D 9 E|
         */
        col[0] = me[t[0]] ^ mb[t[1]] ^ md[t[2]] ^ m9[t[3]];
        col[1] = m9[t[0]] ^ me[t[1]] ^ mb[t[2]] ^ md[t[3]];
        col[2] = md[t[0]] ^ m9[t[1]] ^ me[t[2]] ^ mb[t[3]];
        col[3] = mb[t[0]] ^ md[t[1]] ^ m9[t[2]] ^ me[t[3]];
    }
}

static inline uint8_t *as8(void *array) {
    return (uint8_t *) array;
}

static inline uint32_t *as32(void *array) {
    return (uint32_t *) array;
}

static inline uint64_t *as64(void *array) {
    return (uint64_t *) array;
}

/*
 * This is all debugging stuff to make sure the encryption and decryption outputs match.
 */

static void store_step(unsigned char *matrix, const char *where, int sub_num, int sub_total) {
    int num_steps = curr_block->block_num_steps;
    if (num_steps >= __STEPS_MAX) {
        log_error("crypto module step data overflow. Exiting...");
        exit(1);
    }
    step_t *s = &(curr_block->block_steps[num_steps]);
    memcpy(s->step_matrix, matrix, 16);
    strncpy(s->step_name, where, __STEP_NAME_MAX);
    s->step_sub_num = sub_num;
    s->step_sub_total = sub_total;
    log_debug("Stored data for '%s' in round %d of %d", where, sub_num, sub_total);
#ifdef IPSHIELD_CRYPTO_PRINT_FORWARDS
    print_matrix(matrix);
#endif
    curr_block->block_num_steps++;
}

static void check_step(unsigned char *matrix, const char *where) {
    int *ptr_steps = &(curr_block->block_num_steps);
    if (*ptr_steps <= 0) {
        log_error("Decrypt ran out of steps to check for current block. Exiting...");
        exit(1);
    }
    (*ptr_steps)--;
    step_t *s = &(curr_block->block_steps[*ptr_steps]);
    uint32_t *ma = as32(matrix);
    uint32_t *me = as32(s->step_matrix);
    for (int i = 0; i < 4; i++) {
        if (ma[i] != me[i]) {
            log_debug("Crypto self-test fail for '%s', corresponding to '%s' for round %d of %d",
                      where, s->step_name, s->step_sub_num, s->step_sub_total);
            log_debug("Expected matrix:");
            print_matrix(s->step_matrix);
            log_debug("Actual matrix:");
            print_matrix(matrix);
            log_error("Self-test fail. Exiting...");
            exit(1);
        }
    }
    log_debug("Data match for '%s', corresponding to '%s' for round %d of %d",
              where, s->step_name, s->step_sub_num, s->step_sub_total);
#ifdef IPSHIELD_CRYPTO_PRINT_BACKWARDS
    print_matrix(matrix);
#endif
    if (*ptr_steps == 0) {
        log_debug("Last step passed in self-test for current block.");
    }
}

static void print_matrix(unsigned char *m) {
    char *div = "---------------------";
    char *line = "| %02X | %02X | %02X | %02X |";
    log_debug(div);
    // again note matrix is column-major
    for (int r = 0; r < 4; r++) {
        log_debug(line, m[r+0], m[r+4], m[r+8], m[r+12]);
        log_debug(div);
    }
}