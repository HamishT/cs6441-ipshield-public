/*
 * Net filter library handling.
 */

#include <stdlib.h>
// The libnetfilter authors used stdint but didn't include it!
#include <stdint.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include "queue_manager.h"
#include "logger.h"

int queue_attach(struct nfq_handle **h,
                  nfq_callback *cb_in,
                  nfq_callback *cb_out,
                  struct nfq_q_handle **qh_in,
                  struct nfq_q_handle **qh_out,
                  int *sock){

    if ((*h = nfq_open()) == NULL) {
        log_error("failed to open nfq library handle");
        return -1;
    }

    if (nfq_unbind_pf(*h, AF_INET)) {
        log_error("failed to unbind previous AF_INET handler");
        return -1;
    }

    if (nfq_bind_pf(*h, AF_INET)) {
        log_error("failed to bind to AF_INET handler");
        return -1;
    }

    if ((*qh_in = nfq_create_queue(*h, 0, cb_in, NULL)) == NULL) {
        log_error("failed to create input queue");
        return -1;
    }
    if ((*qh_out = nfq_create_queue(*h, 1, cb_out, NULL)) == NULL) {
        log_error("failed to create output queue");
    }

    if (nfq_set_mode(*qh_in, NFQNL_COPY_PACKET, 0xffff)) {
        log_error("failed to set input packet copy mode");
        return -1;
    }
    if (nfq_set_mode(*qh_out, NFQNL_COPY_PACKET, 0xffff)) {
        log_error("failed to set output packet copy node");
        return -1;
    }

    *sock = nfq_fd(*h);

    return 0;
}

int queue_detach(struct nfq_handle *h, struct nfq_q_handle *qh_in, struct nfq_q_handle *qh_out) {
    nfq_destroy_queue(qh_in);
    nfq_destroy_queue(qh_out);
    nfq_close(h);
    return 0;
}