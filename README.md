# IPShield #

My submission for UNSW COMP6441's Something Awesome project. Its purpose was to approximate a suite like IPSec, which automatically (i.e. at the network layer) encrypts and decrypts every packet sent between hosts set up to use the system (though I only intended to encrypt TCP). In its final form, the AES functionality was proven (AES-128 in ECB mode, PKCS5 padding) and the ability to mangle packets was proven, though a setup to demonstrate the full functionality in tandem was never finished.

# Contents #

src/: C source for modules and main program

include/: C module headers

test/: contains AES correctess test program (only checks self-consistency; state/key is reported but must be checked against a reference tool)

logs/: example logs of AES correctness in various levels of detail (level 0: no checks, level 1; self-check steps; level 2: self-check steps with state matrix printed every encryption step; level 3: self-check steps with state matrix printed every encryption and decryption step)

CMakeLists.txt: CMake build configuration file

# Requirements #

CMake (at least version 3.7)

libnetfilter_queue