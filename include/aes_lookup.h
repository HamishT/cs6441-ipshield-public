/*
 * Various finite field math operations needed for AES, calculated from lookup tables.
 */

#ifndef IPSHIELD_AES_LOOKUP_H
#define IPSHIELD_AES_LOOKUP_H

// Calculate all the following tables for reference.
void aes_lut_initialise(void);

// Multiplication and division in Rijndael's finite field.
unsigned char aes_multiply(unsigned char a, unsigned char b);
unsigned char aes_divide(unsigned char a, unsigned char b);

// Represents the function f(x) = 2^x in Rijndael's finite field
// Why the designers named it this I don't know.
extern unsigned char aes_rcon[11];

// Forward and backward subsitution boxes
extern unsigned char aes_sbox[256];
extern unsigned char aes_sbox_inv[256];

// Log and antilog tables in finite field using 3 as a generator
extern unsigned char aes_log[256];
extern unsigned char aes_alog[256];

// Frequently accessed multiplication tables
extern unsigned char aes_mul2[256];
extern unsigned char aes_mul3[256];
extern unsigned char aes_mul9[256];
extern unsigned char aes_mulb[256];
extern unsigned char aes_muld[256];
extern unsigned char aes_mule[256];

// Multiplicative inverse (= 1/x)
extern unsigned char aes_mul_inv[256];

#endif //IPSHIELD_AES_LOOKUP_H
