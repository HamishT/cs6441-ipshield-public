/*
 * Interface to handle most of the awkward net filter stuff.
 */

#ifndef IPSHIELD_QUEUE_MANAGER_H
#define IPSHIELD_QUEUE_MANAGER_H

#include <stdint.h>
#include <libnetfilter_queue/libnetfilter_queue.h>

int queue_attach(struct nfq_handle **h, nfq_callback *cb_in, nfq_callback *cb_out, struct nfq_q_handle **qh_in, struct nfq_q_handle **qh_out, int *sock);

int queue_detach(struct nfq_handle *h, struct nfq_q_handle *qh_in, struct nfq_q_handle *qh_out);

#endif //IPSHIELD_QUEUE_MANAGER_H
