//
// Created by hamish on 2/05/17.
//

#ifndef IPSHIELD_LOGGER_H
#define IPSHIELD_LOGGER_H

#include <stdarg.h>

void log_info(const char *msg, ...);

void log_debug(const char *msg, ...);

void log_error(const char *msg, ...);

void log_warn(const char *msg, ...);

#endif //IPSHIELD_LOGGER_H
