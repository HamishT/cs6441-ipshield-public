/*
 * AES implementation interface
 */

#ifndef IPSHIELD_CRYPTO_H
#define IPSHIELD_CRYPTO_H

/*
 * Note: implementation has a self-checking debug mode. Defines are:
 * IPSHIELD_CRYPTO_SELF_TEST (test process data step by step)
 * IPSHIELD_CRYPTO_PRINT_FORWARD (print matrices while encrypting)
 * IPSHIELD_CRYPTO_PRINT_BACKWARDS (print matrices while decrypting)
 */

typedef struct {
    unsigned char state[16];
    unsigned char key[16];
    unsigned char roundKeys[11][16];
} crypto_state_t;

void crypto_initialise(crypto_state_t *cs);
int crypto_encrypt(crypto_state_t *cs, void *src, int len, void *dst);
int crypto_decrypt(crypto_state_t *cs, void *src, int len, void *dst);

#endif //IPSHIELD_CRYPTO_H
